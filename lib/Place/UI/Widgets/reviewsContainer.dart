import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:platzi_trips_app/Place/UI/Widgets/review.dart';


class ReviewContainer  extends StatelessWidget{
  @override
  Widget build(BuildContext context) {

    Column columnReviewContainer = Column(crossAxisAlignment: CrossAxisAlignment.start,children: [new Review(5),new Review(4),new Review(3)],);

      Container reviewsContainer = Container(
      child: Container(
        margin: EdgeInsets.only(
            top: 50,
                left: 20
        ),
        width: 320,
        constraints:BoxConstraints(minHeight: 100),
        child: Container(margin: EdgeInsets.only(
            top: 10
        ),
          child: Container(constraints: BoxConstraints(minHeight: 100),child: columnReviewContainer,),),
      ),
    );

        return reviewsContainer;

  }

}