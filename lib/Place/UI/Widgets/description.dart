import 'package:flutter/material.dart';
import 'package:platzi_trips_app/Widgets/button_purple.dart';

class DescriptionPlace  extends StatelessWidget {

  final int total_stars = 5;
  String namePlace;
  int stars;
  String descriptionPlace;

  DescriptionPlace(this.namePlace,this.stars,this.descriptionPlace) {

  }

  List<Container> getStars(int starsOn) {
    List<Container> stars = [];
    final Container starOn = Container(
      margin: EdgeInsets.only(
          right:3.0 
      ),
      child: Icon(Icons.star,color: Color(0xFFf2C611),),
    );
    final Container starOff = Container(
      margin: EdgeInsets.only(
          right:3.0
      ),
      child: Icon(Icons.star_border,color: Color(0xFFf2C611)),
    );


    for(int x = 0; x < this.total_stars;x++) {
      if(x+1 <= starsOn) {
        stars.add(starOn);
      }else {
        stars.add(starOff);
      }
    }

    return stars;
  }

  @override
  Widget build(BuildContext context) {

    final int total_stars = 5;


    Container textChild = Container(
      margin: EdgeInsets.only(
          top: 320.0,
          left: 20.0,
          right: 20.0
      ) ,
      child: Text(this.namePlace,
        style: TextStyle(
            fontSize: 30.0,
            fontWeight: FontWeight.w900,
          fontFamily: "Lato"
        ),
        textAlign: TextAlign.left,
      ),
    );
    List<Container> stars = this.getStars(this.stars);
    Row starsRow = Row(children: [...stars],);

    Container stars_child = Container(
      margin: EdgeInsets.only(
          top: 323.0,
          right:3.0
      ),
      child: starsRow,
    );

    Row starsSection = Row(
      children: <Widget>[textChild,stars_child],
    );

    Column descriptionSection = Column(
      children: [
        Container(margin: EdgeInsets.only(
          top: 30,
          left: 20
        ),child: Text(this.descriptionPlace,style: TextStyle(
          fontFamily: 'Lato'
        ),),width: 320),

      ],
    );

    final Column title_stars = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
      starsSection,descriptionSection,new ButtonPurple('Navigate')
    ],);


    return title_stars;
  }

}