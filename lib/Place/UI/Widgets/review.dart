import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class Review extends StatelessWidget{

  final int total_stars = 5;
  int stars_on;

  Review(this.stars_on) {}


  List<Container> getStars(int starsOn) {
    List<Container> stars = [];
    final Container starOn = Container(
      width: 2,
      margin: EdgeInsets.only(
          right:15.0
      ),
      child: Icon(Icons.star,color: Color(0xFFf2C611),),
    );
    final Container starOff = Container(
      width: 2,
      margin: EdgeInsets.only(
          right:15.0
      ),
      child: Icon(Icons.star_border,color: Color(0xFFf2C611)),
    );


    for(int x = 0; x < this.total_stars;x++) {
      if(x+1 <= starsOn) {
        stars.add(starOn);
      }else {
        stars.add(starOff);
      }
    }

    return stars;
  }


  @override
  Widget build(BuildContext context) {

    Container containerImageReview = Container(
      width: 80,
      constraints: BoxConstraints(minHeight: 100),
      decoration:BoxDecoration(
        shape: BoxShape.circle,
        image: DecorationImage(image:AssetImage('assets/images/tienda-oficial-del-fc-barcelona.jpg') ,fit: BoxFit.cover)
      ) ,
    );

    Container containerInfoReview = Container(
      padding: EdgeInsets.only(left: 10),
      width: 240,
      constraints: BoxConstraints(minHeight: 100),
      child: Column(
        children: [
          Container(
            width: 240,
            margin: EdgeInsets.only(top: 10,bottom: 10),
            child: Text('Varuna Yasas',style: TextStyle(fontSize: 20),),
          ),
          Container(
            width: 240,
            margin: EdgeInsets.only(bottom: 10),
            child: Row(
              children: [
                Container(child: Text('1 Review',style: TextStyle(color: Colors.grey),),),
                Container(margin: EdgeInsets.only(left: 5),child: Text('- 5 photos',style: TextStyle(color: Colors.grey)),),
                Container(
                  margin: EdgeInsets.only(left: 5),
                  child: Row(
                    children: [...this.getStars(this.stars_on)],
                  ),
                )
              ],
            ),
          ),
          Container(
            width: 240,
            margin: EdgeInsets.only(bottom: 10),
            child: Text('There is an amazing place in Sri lanka',style: TextStyle(fontWeight: FontWeight.bold)),
          )
        ],
      ),
    );

    Row rowReview =  Row(children: [containerImageReview,containerInfoReview],);

    Container reviewContainer = Container(
      width:320 ,
      constraints: BoxConstraints(minHeight: 100),
      margin: EdgeInsets.only(
        bottom: 10
      ),
      child: rowReview,
    );

    return reviewContainer;

  }

  
}