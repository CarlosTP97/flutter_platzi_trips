import 'package:flutter/material.dart';
import 'package:platzi_trips_app/Place/UI/Widgets/card_image.dart';


class CardImageList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return Container(
      height: 350.0,
      child: ListView(
        padding: EdgeInsets.all(25.0),
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          new CardImage('assets/images/tienda-oficial-del-fc-barcelona.jpg'),
          new CardImage('assets/images/tienda-oficial-del-fc-barcelona.jpg'),
          new CardImage('assets/images/tienda-oficial-del-fc-barcelona.jpg'),
          new CardImage('assets/images/tienda-oficial-del-fc-barcelona.jpg'),
          new CardImage('assets/images/tienda-oficial-del-fc-barcelona.jpg')
        ],
      ) ,
    );

  }

}