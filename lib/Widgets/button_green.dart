import 'package:flutter/material.dart';

class ButtonGrenn extends StatefulWidget {
  final String text;
  double width;
  double height;
  VoidCallback onPressed;

  ButtonGrenn(
      {Key key,
      @required this.text,
      @required this.onPressed,
      this.width,
      this.height});

  @override
  State<StatefulWidget> createState() {
    return new _ButtonGrenn();
  }
}

class _ButtonGrenn extends State<ButtonGrenn> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: InkWell(
        onTap: widget.onPressed,
        child: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [Color(0xFFa7ff84), Color(0xFF1cbb78)]),
              borderRadius: BorderRadius.circular(20.0)),
          width: widget.width,
          height: widget.height,
          alignment: Alignment(0, 0),
          child: Text(
            widget.text,
            style: TextStyle(
                color: Colors.white,
                fontFamily: 'Lato',
                fontSize: 20,
                fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }
}
