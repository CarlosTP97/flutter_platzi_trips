import 'package:flutter/material.dart';

class FloatingActionButtonGreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _FloatingActionButtonGreen();
  }
}

class _FloatingActionButtonGreen extends State<FloatingActionButtonGreen> {
  Icon floatingButtonIcon = Icon(Icons.favorite_border);
  bool addedToFav = false;

  void onPressedFav() {
    setState(() {
      if (!this.addedToFav) {
        this.floatingButtonIcon = Icon(Icons.favorite);
      } else {
        this.floatingButtonIcon = Icon(Icons.favorite_border);
      }
      this.addedToFav = !this.addedToFav;
    });
  }

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      backgroundColor: Color(0xFF11DA53),
      mini: true,
      tooltip: "Fav",
      onPressed: this.onPressedFav,
      child: this.floatingButtonIcon,
    );
  }
}
