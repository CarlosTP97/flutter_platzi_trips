import 'package:flutter/material.dart';
import 'package:platzi_trips_app/Place/UI/Widgets/card_image_list.dart';
import 'package:platzi_trips_app/Widgets/gradient_back.dart';

class HeaderAppbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return Stack(
      children: [
        new GradientBack('Popular',250),
        new CardImageList()
      ],
    );

  }


}