import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:generic_bloc_provider/generic_bloc_provider.dart';
import 'package:platzi_trips_app/User/Repository/auth_repository.dart';

class UserBloc implements Bloc {
  final AuthRepository _authRepository = new AuthRepository();

  Stream<User> streamFirebase = FirebaseAuth.instance.authStateChanges();
  User user = FirebaseAuth.instance.currentUser;

  Stream<User> getAuthStatus() {
    this.streamFirebase.listen((event) {
      print('auth perron: ' + event.toString());
    });
    return this.streamFirebase;
  }

  Future<UserCredential> signIn() async {
    return await this._authRepository.signInFirebase();
  }

  Future<void> signOut() async {
    return await this._authRepository.signOutFirebase();
  }

  @override
  void dispose() {}
}
