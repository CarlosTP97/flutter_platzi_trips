import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_core/firebase_core.dart';

class FirebaseAuthAPI {
  FirebaseAuth _auth = FirebaseAuth.instance;
  GoogleSignIn googleSignIn = GoogleSignIn();

  Future<UserCredential> signIn() async {
    GoogleSignInAccount googleSignInAccount = await this.googleSignIn.signIn();

    GoogleSignInAuthentication gsa = await googleSignInAccount.authentication;

    UserCredential user;
    try {
      user = await this._auth.signInWithCredential(
          GoogleAuthProvider.credential(
              idToken: gsa.idToken, accessToken: gsa.accessToken));
    } catch (e) {}

    return user;
  }

  Future<void> signOut() async {
    await this._auth.signOut();
    await this.googleSignIn.signOut();
  }
}
