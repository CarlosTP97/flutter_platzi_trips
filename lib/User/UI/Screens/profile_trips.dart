import 'package:flutter/material.dart';
import 'package:generic_bloc_provider/generic_bloc_provider.dart';
import 'package:platzi_trips_app/User/Bloc/bloc_user.dart';
import 'package:platzi_trips_app/User/UI/Widgets/body_profile.dart';
import 'package:platzi_trips_app/Widgets/gradient_back.dart';

class ProfileTrips extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Container containerProfile = Container(
        constraints:
            BoxConstraints(minHeight: MediaQuery.of(context).size.height * 1),
        child: Stack(
          children: [
            Stack(
              children: [new GradientBack('Profile', 350), new BodyProfile()],
            )
          ],
        ));

    return containerProfile;
  }
}
