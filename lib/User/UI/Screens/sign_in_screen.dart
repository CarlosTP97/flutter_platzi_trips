import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:generic_bloc_provider/generic_bloc_provider.dart';
import 'package:platzi_trips_app/User/Bloc/bloc_user.dart';
import 'package:platzi_trips_app/Widgets/button_green.dart';
import 'package:platzi_trips_app/Widgets/gradient_back.dart';
import 'package:platzi_trips_app/platzi_trips.dart';

class SignInScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _SignInScreen();
  }
}

class _SignInScreen extends State<SignInScreen> {
  UserBloc _userBloc;

  @override
  Widget build(BuildContext context) {
    this._userBloc = BlocProvider.of(context);
    return this._handleCurrentSession();
  }

  Widget _handleCurrentSession() {
    return StreamBuilder(
      stream: this._userBloc.getAuthStatus(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (!snapshot.hasData || snapshot.hasError) {
          return this.signInGoogleUI();
        } else {
          return PlatziTrips();
        }
      },
    );
  }

  Widget signInGoogleUI() {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          new GradientBack('', null),
          Container(
            margin: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width * 1,
                  child: Text(
                    "Welcome. \nThis is your Travel App.",
                    style: TextStyle(
                        fontSize: 30.0,
                        fontFamily: 'Lato',
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.start,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 50),
                  child: new ButtonGrenn(
                    key: Key('key'),
                    text: 'Login with Gmail',
                    onPressed: this._userBloc.signIn,
                    width: null,
                    height: 60,
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
