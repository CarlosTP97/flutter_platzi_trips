import 'package:flutter/material.dart';

class ProfileMenuItem extends StatelessWidget {
  Icon icon = null;
  VoidCallback onPressed;

  ProfileMenuItem(this.icon, this.onPressed);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 50,
      child: FloatingActionButton(
        backgroundColor: Colors.white,
        mini: true,
        tooltip: 'Fav',
        child: this.icon,
        onPressed: this.onPressed,
      ),
    );
  }
}
