import 'package:flutter/material.dart';
import 'package:generic_bloc_provider/generic_bloc_provider.dart';
import 'package:platzi_trips_app/User/Bloc/bloc_user.dart';
import 'package:platzi_trips_app/User/UI/Widgets/profile_card.dart';
import 'package:platzi_trips_app/User/UI/Widgets/profile_menu.dart';
import 'package:platzi_trips_app/User/UI/Widgets/profile_places_list.dart';

class BodyProfile extends StatelessWidget {
  UserBloc _userBloc;
  @override
  Widget build(BuildContext context) {
    this._userBloc = BlocProvider.of<UserBloc>(context);
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(top: 130, left: 20, right: 20),
          child: Column(
            children: [
              new ProfileCard(this._userBloc.user.displayName,
                  this._userBloc.user.email, this._userBloc.user.photoURL),
              new ProfileMenu(this._userBloc),
            ],
          ),
        ),
        new ProfilePlacesList()
      ],
    );
  }
}
