import 'package:flutter/material.dart';
import 'package:platzi_trips_app/Widgets/floating_action_button_green.dart';

class ProfilePlaceItem extends StatelessWidget {

String imagePath = '';
String title = '';
String direction = '';
String steps = '';

ProfilePlaceItem(this.imagePath,this.title,this.direction,this.steps);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 250,
      margin: EdgeInsets.only(
        bottom: 20
      ),
      child: Stack(
       alignment: Alignment(0,-1),
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              image: DecorationImage(
                image: AssetImage(this.imagePath),
                fit: BoxFit.cover
              )
            ), 
            height: 200
          ),
          Container(
            margin: EdgeInsets.only(
              top: 150
            ),
            width: 250,
            height: 100,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              boxShadow:<BoxShadow> [
                BoxShadow(
                  color: Colors.black38,
                  blurRadius: 15.0,
                  offset: Offset(0.0,7.0)
                )
              ]  
            ),
            child: Stack(
              children: [
                 Container(
                   
                  margin: EdgeInsets.only(
                    left: 20,
                    right: 20,
                    top: 10,
                    bottom: 10
                  ),
                  width: 250.0,
                  child: Column(
                    children: [
                      Container(
                         width: 250.0,
                        height: 30,
                        child: Text(
                          this.title,
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w900
                          ),
                        ),
                      ),
                      Container(
                        width: 250.0,
                        height: 30,
                        child: Text(
                          this.direction,
                          style: TextStyle(
                            fontSize: 12,
                           color: Colors.grey
                          ),
                        ),
                      ),
                      Container(
                         width: 250.0,
                        height: 20,
                        child: Text(
                          this.steps,
                          style: TextStyle(
                            color: Colors.orange
                          ),
                        )
                      )
                    ],
                  ),
                ),
                Container(
                  width: 250.0,
                  alignment: Alignment(1,1.3),
                  child: new FloatingActionButtonGreen(),
                  
                )
              ],
            )
          ),
        ],
      ),
    );
    
  }


}