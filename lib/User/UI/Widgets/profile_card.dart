import 'package:flutter/material.dart';

class ProfileCard extends StatelessWidget {
  String name = '';
  String email = '';
  String pathImage = '';

  ProfileCard(this.name, this.email, this.pathImage);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      child: Row(
        children: [
          Container(
            width: MediaQuery.of(context).size.width * .20,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(color: Colors.white, width: 2),
                image: DecorationImage(
                    image: NetworkImage(this.pathImage), fit: BoxFit.cover)),
          ),
          Container(
            margin: EdgeInsets.only(top: 30, left: 10),
            width: MediaQuery.of(context).size.width * .60,
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(bottom: 5),
                  width: MediaQuery.of(context).size.width * .68,
                  height: 20,
                  child: Text(
                    this.name,
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * .68,
                  height: 20,
                  child: Text(
                    this.email,
                    style: TextStyle(color: Colors.grey, fontSize: 15),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
