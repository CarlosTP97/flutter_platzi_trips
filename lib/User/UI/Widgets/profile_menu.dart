import 'package:flutter/material.dart';
import 'package:platzi_trips_app/User/Bloc/bloc_user.dart';
import 'package:platzi_trips_app/User/UI/Widgets/profile_menu_item.dart';

class ProfileMenu extends StatelessWidget {
  UserBloc _userBloc;

  ProfileMenu(this._userBloc);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          new ProfileMenuItem(Icon(Icons.exit_to_app, color: Colors.blue),
              this._userBloc.signOut),
          new ProfileMenuItem(Icon(Icons.add, color: Colors.blue), null),
        ],
      ),
    );
  }
}
