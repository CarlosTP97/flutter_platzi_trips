import 'package:flutter/material.dart';
import 'package:platzi_trips_app/Place/UI/Screens/home_trips.dart';
import 'package:platzi_trips_app/User/UI/Screens/profile_trips.dart';
import 'package:platzi_trips_app/Search/UI/Screens/search_trips.dart';


class PlatziTrips extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _PlatziTrips();
  }

}

class _PlatziTrips extends State<PlatziTrips> {

  int indexTap = 0;

  final List<Widget> widgetsChildren = [
    new HomeTrips(),
    new SearchTrips(),
    new ProfileTrips()
  ];


  void onTapTapped(int index) {
    setState(() {
      this.indexTap = index;
    });
  }

  @override
  Widget build(BuildContext context) {

    Scaffold body = Scaffold(
      body: widgetsChildren[this.indexTap],
      bottomNavigationBar: Theme(
        data: Theme.of(context).copyWith( 
          canvasColor: Colors.white,
          primaryColor: Colors.purple
        ),
        child: BottomNavigationBar(
          onTap: onTapTapped,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label:'',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.search),
              label:'' 
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label:'' 
            )
          ],
        ),
      ),
    );

    return body;

  }



}